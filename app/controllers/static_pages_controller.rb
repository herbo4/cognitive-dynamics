class StaticPagesController < ApplicationController
  def home
    ##configure the BitBucket Feed##
    #use the url of the rss feed you want
    url= "https://bitbucket.org/herbo4/cognitive-dynamics/rss?token=922db50ce656e58771d08c34d4123a4d"
    #declare a symbol to reference the feed from using Feedjira
    @recent =  Feedjira::Feed.fetch_and_parse url
  end

  def blog
  end

  def projects
  end

  def team
  end

  def social
  end
end
