require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  def setup
    @base_title = "Cognitive Dynamics"
  end
  
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get blog" do
    get :blog
    assert_response :success
    assert_select "title", "Blog | #{@base_title}"
  end

  test "should get projects" do
    get :projects
    assert_response :success
    assert_select "title", "Projects | #{@base_title}"
  end

  test "should get team" do
    get :team
    assert_response :success
    assert_select "title", "Dev Team | #{@base_title}"
  end

  test "should get social" do
    get :social
    assert_response :success
    assert_select "title", "Social Media | #{@base_title}"
  end

end
