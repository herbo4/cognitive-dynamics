```
#!asciiart

   ____                  _ _   _               
  / ___|___   __ _ _ __ (_) |_(_)_   _____     
 | |   / _ \ / _` | '_ \| | __| \ \ / / _ \    
 | |__| (_) | (_| | | | | | |_| |\ V /  __/    
  \____\___/ \__, |_| |_|_|\__|_| \_/ \___|    
  ____       |___/                  _          
 |  _ \ _   _ _ __   __ _ _ __ ___ (_) ___ ___ 
 | | | | | | | '_ \ / _` | '_ ` _ \| |/ __/ __|
 | |_| | |_| | | | | (_| | | | | | | | (__\__ \
 |____/ \__, |_| |_|\__,_|_| |_| |_|_|\___|___/
        |___/                                  

```

Welcome to the Cognitive Dynamics web site. We keep all of our projects here, as
well as information about who we are and what we enjoy doing. We have our social
media hooked into the site, so you can catch our twitter, facebook, and google+
updates here. Feel free to look around and have a good time.