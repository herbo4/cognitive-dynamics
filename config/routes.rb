Rails.application.routes.draw do
  root 'static_pages#home'
  get 'team'   => 'static_pages#team'
  get 'social'   => 'static_pages#social'
  get 'blog'  => 'static_pages#blog'
  get 'projects'  => 'static_pages#projects'
  get 'signup'  => 'users#new'
  resources :users
end
